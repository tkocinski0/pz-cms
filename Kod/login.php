<html lang="pl">
<head>
<meta charset="utf-8"/>
	<title>CMS BLOG</title>
	<meta name="description" content="Załóż własny BLOG dzięki naszemu CMS"/>
	<meta name="keywords" content="cms, blog"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

<div id="container">
	<div id="logo">
		<b>CMS BLOG</b>
	</div>
<center>
	<div id="text">

		<h2>Uzupełnij swoje dane do logowania:</h2>
		<br />
		<form method="POST">
			Login:
			<input type="text" maxlength="15" name="login" />
			<br /><br />
			Haslo:
			<input type="text" maxlength="15" name="haslo" />
			<br /><br />
			
			<input type="submit" value="Zaloguj" name="zaloguj"/>
			<br /><br />
			</form>
			
	</div>
	
	<?php
	session_start();
	unset($_SESSION['komentator_zalogowany']);
	unset($_SESSION['bloger_zalogowany']);
	session_destroy();
	
	include 'funkcje.php';
	if(isset($_POST['zaloguj'])){
		$login = $_POST['login'];
		$haslo = $_POST['haslo'];
		logowanie($login, $haslo);
	}
	
	?>
	
</center>
	<div id="stopka">&copy; cms blog</div>
</div>
</body>

</html>