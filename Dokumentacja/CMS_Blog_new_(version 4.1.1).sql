-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2015-05-27 20:33:13.158



-- tables
-- Table: Blog
CREATE TABLE Blog (
    ID_bloga integer  NOT NULL AUTO_INCREMENT,
    nazwa_bloga char(255)  NOT NULL,
    tlo char(255)  NOT NULL,
    banner char(255)  NOT NULL,
    CONSTRAINT Blog_pk PRIMARY KEY (ID_bloga)
) ;




-- Table: Bloger
CREATE TABLE Bloger (
    ID_blogera integer  NOT NULL AUTO_INCREMENT,
    login char(255)  NOT NULL,
    haslo char(255)  NOT NULL,
    data_rejestracji date  NOT NULL,
    ID_bloga integer  NOT NULL,
    CONSTRAINT Bloger_pk PRIMARY KEY (ID_blogera)
) ;




-- Table: Bloger_komentarz
CREATE TABLE Bloger_komentarz (
    ID_komentarza integer  NOT NULL AUTO_INCREMENT,
    tresc char(255)  NOT NULL,
    ID_blogera integer  NOT NULL,
    ID_wpisu integer  NOT NULL,
    data date  NOT NULL,
    akceptacja integer  NOT NULL,
    CONSTRAINT Bloger_komentarz_pk PRIMARY KEY (ID_komentarza)
) ;




-- Table: Komentator
CREATE TABLE Komentator (
    ID_komentatora integer  NOT NULL AUTO_INCREMENT,
    login char(255)  NOT NULL,
    haslo char(255)  NOT NULL,
    data_rejestracji date  NOT NULL,
    CONSTRAINT Komentator_pk PRIMARY KEY (ID_komentatora)
) ;




-- Table: Komentator_komentarz
CREATE TABLE Komentator_komentarz (
    ID_komentarza integer  NOT NULL AUTO_INCREMENT,
    tresc char(255)  NOT NULL,
    data date  NOT NULL,
    ID_wpisu integer  NOT NULL,
    ID_komentatora integer  NOT NULL,
    akceptacja integer  NOT NULL,
    CONSTRAINT Komentator_komentarz_pk PRIMARY KEY (ID_komentarza)
) ;




-- Table: Tag
CREATE TABLE Tag (
    ID_tag integer  NOT NULL AUTO_INCREMENT,
    nazwa_tagu char(255)  NOT NULL,
    ID_bloga integer  NOT NULL,
    CONSTRAINT Tag_pk PRIMARY KEY (ID_tag)
) ;




-- Table: Wpis
CREATE TABLE Wpis (
    ID_wpisu integer  NOT NULL AUTO_INCREMENT,
    tresc char(255)  NOT NULL,
    tytul char(255)  NOT NULL,
    ID_bloga integer  NOT NULL,
    ukryty integer  NOT NULL,
    moderacja integer  NOT NULL,
    ID_tag integer  NOT NULL,
    CONSTRAINT Wpis_pk PRIMARY KEY (ID_wpisu)
) ;








-- foreign keys
-- Reference:  Bloger_Blog (table: Bloger)


ALTER TABLE Bloger ADD CONSTRAINT Bloger_Blog 
    FOREIGN KEY (ID_bloga)
    REFERENCES Blog (ID_bloga)
    ;

-- Reference:  Bloger_komentarz_Bloger (table: Bloger_komentarz)


ALTER TABLE Bloger_komentarz ADD CONSTRAINT Bloger_komentarz_Bloger 
    FOREIGN KEY (ID_blogera)
    REFERENCES Bloger (ID_blogera)
    ;

-- Reference:  Bloger_komentarz_Wpis (table: Bloger_komentarz)


ALTER TABLE Bloger_komentarz ADD CONSTRAINT Bloger_komentarz_Wpis 
    FOREIGN KEY (ID_wpisu)
    REFERENCES Wpis (ID_wpisu)
    ;

-- Reference:  ID_komentatora (table: Komentator_komentarz)


ALTER TABLE Komentator_komentarz ADD CONSTRAINT ID_komentatora 
    FOREIGN KEY (ID_komentatora)
    REFERENCES Komentator (ID_komentatora)
    ;

-- Reference:  Komentator_komentarz_Wpis (table: Komentator_komentarz)


ALTER TABLE Komentator_komentarz ADD CONSTRAINT Komentator_komentarz_Wpis 
    FOREIGN KEY (ID_wpisu)
    REFERENCES Wpis (ID_wpisu)
    ;

-- Reference:  Tag_Blog (table: Tag)


ALTER TABLE Tag ADD CONSTRAINT Tag_Blog 
    FOREIGN KEY (ID_bloga)
    REFERENCES Blog (ID_bloga)
    ;

-- Reference:  Wpis_Blog (table: Wpis)


ALTER TABLE Wpis ADD CONSTRAINT Wpis_Blog 
    FOREIGN KEY (ID_bloga)
    REFERENCES Blog (ID_bloga)
    ;

-- Reference:  Wpis_Tag (table: Wpis)


ALTER TABLE Wpis ADD CONSTRAINT Wpis_Tag 
    FOREIGN KEY (ID_tag)
    REFERENCES Tag (ID_tag)
    ;





-- End of file.

